/* Shelf label.

The clips on this shelf label allow it to be attached to wire bakers' (NSF) shelves.  The width of the label is adjustable, so it can be used with wire shelves of various sizes.

(c) 2023 Michael Ashton */

/* All units in mm */

// Length of label
length=80.0;

// Width of label.  For wire shelves, this should be the inside distance between the upper wire and lower wire on a shelf.
width=16.0; //[10:0.1:30]

// Thickness of label
thickness=3.0;

// Radius of rounded corners
corner_radius=2;

// Amount to increase the width of label on top and bottom, without moving the clip position
extra_border=0.6;

clip_length=8;
clip_width=6;

clip_thickness=3;

// Height of the protrusion peak
clip_protrusion=0.25;

// Distance from the end of the clip to the peak of the protrusion
clip_protrusion_peak=3;

clip_offset=15;

// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Transformations#offset
module fillet(r) {
   offset(r = -r) {
     offset(delta = r) {
       children();
     }
   }
}

module base() {
    linear_extrude(height=thickness)
    fillet(-corner_radius)
    square([length, width + extra_border], center = true);
}

module clip() {
    rotate(a = [-90, 0, -90])
    linear_extrude(height = clip_width) {
        polygon(points = [
            [0, 0],
            [0, clip_length],
            [clip_thickness, clip_length],
            [clip_thickness + clip_protrusion, clip_length - clip_protrusion_peak],
            [clip_thickness, clip_length - (clip_protrusion_peak * 2)],
            [clip_thickness, 0]
        ]);
    }
}

module clip_top() {
    translate([-clip_width/2, clip_thickness - width/2, 0])
    clip();
}

module clip_bot() {
    translate([clip_width/2, width/2 - clip_thickness, 0])
    rotate(a=[0,0,-180])
    clip();
}

module clips() {
    clip_bot();
    translate([clip_offset, 0, 0]) clip_top();
    translate([-clip_offset, 0, 0]) clip_top();
}

base();
clips();
